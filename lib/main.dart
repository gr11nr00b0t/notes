import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:notes/app_data.dart';

import 'data/entity/notes/notes_entity_impl.dart';

void main() {
  AppData appData = initAppData();

  runApp(
    MyApp(appData: appData),
  );
}

class MyApp extends StatelessWidget {
  final AppData appData;

  const MyApp({Key? key, required this.appData}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<NotesEntityImpl>(
          create: (context) => NotesEntityImpl(appData.notesDatabaseRepository),
        ),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const MyHomePage(),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return const Scaffold();
  }
}
