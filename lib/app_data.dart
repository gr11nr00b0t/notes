
import 'package:notes/data/entity/notes/notes_entity.dart';

import 'data/entity/notes/notes_entity_impl.dart';
import 'data/repository/database/notes/notes_database_repository.dart';
import 'data/repository/database/notes/notes_database_repository_impl.dart';

class AppData {

  NotesDatabaseRepository notesDatabaseRepository;
  NotesEntity notesEntity;
  
  AppData(
      this.notesDatabaseRepository,
      this.notesEntity,
      );
}

AppData initAppData(){

  NotesDatabaseRepository notesDatabaseRepository = NotesDatabaseRepositoryImpl();
  NotesEntity notesEntity = NotesEntityImpl(notesDatabaseRepository);

  return AppData(
    notesDatabaseRepository,
    notesEntity,
  );
}