import 'package:notes/model/notes_model.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

import 'notes_database_repository.dart';

class NotesDatabaseRepositoryImpl implements NotesDatabaseRepository {
  static String databaseName = 'DatabaseName.db';
  static int databaseVersion = 1;
  static String tableName = 'TableName';

  static final String databaseExecutor =
      'create table $tableName (id integer primary key autoincrement, title text not null, description text)';

  @override
  Future addNotes(NotesModel notesModel) async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, databaseName);
    Database _database = await openDatabase(path, version: databaseVersion,
        onCreate: (Database db, int version) async {
      await db.execute(databaseExecutor);
    });
    notesModel.id = await _database.insert(
      tableName,
      notesModel.toMap(),
    );
  }

  @override
  Future<int> removeNotes(int id) async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, databaseName);
    Database _database = await openDatabase(path, version: databaseVersion,
        onCreate: (Database db, int version) async {
      await db.execute(databaseExecutor);
    });
    return await _database.delete(
      tableName,
      where: 'id = ?',
      whereArgs: [id],
    );
  }

  @override
  Future<NotesModel?> getNotesBy(int id) async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, databaseName);
    Database _database = await openDatabase(path, version: databaseVersion,
        onCreate: (Database db, int version) async {
      await db.execute(databaseExecutor);
    });

    List<Map> maps = await _database.query(
      tableName,
      columns: [
        'id',
        'name',
        'description',
      ],
      where: 'id = ?',
      whereArgs: [id],
    );
    if (maps.isNotEmpty) {
      return NotesModel.fromMap(maps.first);
    }
    return null;
  }

  @override
  Future<int> updateNotes(NotesModel model) async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, databaseName);
    Database _database = await openDatabase(path, version: databaseVersion,
        onCreate: (Database db, int version) async {
      await db.execute(databaseExecutor);
    });
    return await _database.update(
      tableName,
      model.toMap(),
      where: 'id = ?',
      whereArgs: [model.id],
    );
  }

  @override
  Future<List<NotesModel>> getNotesList() async {
    var databasesPath = await getDatabasesPath();
    String path = join(databasesPath, databaseName);
    Database _database = await openDatabase(path, version: databaseVersion,
        onCreate: (Database db, int version) async {
      await db.execute(databaseExecutor);
    });
    final List<Map<String, dynamic>> maps = await _database.query(tableName);
    return List.generate(maps.length, (i) {
      NotesModel model = NotesModel(
        title: '',
        description: '',
        id: 0,
      );
      model.title = maps[i]['title'];
      model.description = maps[i]['description'];
      return model;
    });
  }

  @override
  Future removeDatabase() async {
    var _databasesPath = await getDatabasesPath();
    String dbPath = join(_databasesPath, databaseName);
    await deleteDatabase(dbPath);
  }
}
