import 'package:notes/data/repository/database/notes/notes_database_repository.dart';
import 'package:notes/model/notes_model.dart';

import 'notes_entity.dart';

class NotesEntityImpl implements NotesEntity {

  late final NotesDatabaseRepository _notesDatabaseRepository;
  NotesEntityImpl(this._notesDatabaseRepository);


  @override
  Future addNotes(NotesModel notesModel) async {
    return await _notesDatabaseRepository.addNotes(notesModel);
  }

  @override
  Future<NotesModel?> getNotesBy(int id) async {
    return await _notesDatabaseRepository.getNotesBy(id);
  }

  @override
  Future<List<NotesModel>> getNotesList() async {
    return await _notesDatabaseRepository.getNotesList();
  }

  @override
  Future removeDatabase() async {
    return await _notesDatabaseRepository.removeDatabase();
  }

  @override
  Future<int> removeNotes(int id) async {
    return await _notesDatabaseRepository.removeNotes(id);
  }

  @override
  Future<int> updateNotes(NotesModel notesModel) async {
    return await _notesDatabaseRepository.updateNotes(notesModel);
  }
}