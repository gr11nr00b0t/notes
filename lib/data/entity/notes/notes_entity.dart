import 'package:notes/model/notes_model.dart';

abstract class NotesEntity {

  Future addNotes(NotesModel notesModel);

  Future<int> removeNotes(int id);

  Future<NotesModel?> getNotesBy(int id);

  Future<int> updateNotes(NotesModel model);

  Future<List<NotesModel>> getNotesList();

  Future removeDatabase();
  
}