class NotesModel {
  int id = 0;
  String title = "";
  String description = "";

  NotesModel({
    required this.id,
    required this.title,
    required this.description,
  });

  Map<String, dynamic> toMap() {
    var map = <String, dynamic>{
      'title': title,
      'description': description,
    };
    map['id'] = id;
    return map;
  }

  NotesModel.fromMap(Map<dynamic, dynamic> map) {
    id = map['id'];
    title = map['title'];
    description = map['description'];
  }
}
